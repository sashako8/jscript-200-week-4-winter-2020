const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  constructor(name) {
    this.name = name;
    this.hand = [];
  }
    drawCard() {
      const randomCard = Math.floor(Math.random() * 51) + 1;
      this.hand.push(blackjackDeck[randomCard]);
    };
  }

// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer ('Dealer'); // TODO
const player = new CardPlayer('Player'); // TODO

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  const blackJackScore = {
    total: 0,
    isSoft: false
  }
  for (i = 0; i < hand.length; i++) {
    if (hand[i].displayVal === 'Ace' && blackJackScore.total >= 11) {
      hand[i].val = 1;
    } else if (hand[i].val === 11) {
      blackJackScore.isSoft = true;
    }
    blackJackScore.total += hand[i].val;
  }
  return blackJackScore;
}

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  totalScore = 0;
  shouldDrawCard = false;
  const numberOfAces = dealerHand.filter((dealerHand) => dealerHand.val === 11);
  for (i = 0; i < dealerHand.length; i++) {
    totalScore += dealerHand[i].val;
  }
  if (totalScore <= 16) {
    shouldDrawCard = true;
  } else if (totalScore === 17 && numberOfAces.length === 1) {
    shouldDrawCard = true;
  } else {
    shouldDrawCard = false;
  }
  return shouldDrawCard;
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  console.log(`Player's score: ${playerScore}, Dealer's score: ${dealerScore}`);
  if (playerScore > dealerScore) {
    console.log('Player wins.');
   } else if (playerScore < dealerScore) {
    console.log('Dealer wins.');
   } else {
     console.log('It\'s a tie.')
   }
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
console.log(startGame());